To compile this code into a shared object, use

#+BEGIN_SRC sh
cargo build
#+END_SRC

This will create a file called =libfib_rs.d= under =target/debug=.

You can symlink this to =fib.so= in the current directory to make loading into
Emacs similar to the C version.
